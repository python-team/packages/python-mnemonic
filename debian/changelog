python-mnemonic (0.21-2) unstable; urgency=medium

  * Add debian/docs to install the upstream README.rst.
  * debian/lintian-overrides:  Fix typo in comment.

 -- Soren Stoutner <soren@debian.org>  Mon, 07 Oct 2024 12:44:25 -0700

python-mnemonic (0.21-1) unstable; urgency=medium

  * New upstream release.
  * Add debian/gbp.conf.
  * Add debian/lintian-overrides.
  * Add debian/pybuild.testfiles.
  * debian/control:
    - Add myself to uploaders.
    - Remove Richard Ulrich <richi@paraeasy.ch> from uploaders at his request.
    - Add Build-Depends on the following packages:
        - pybuild-plugin-pyproject
        - python3-poetry-core.
        - python3-pytest <!nocheck>
    - Change Testsuite from autopkgtest-pkg-python to autopkgtest-pkg-pybuild.
    - Bump standards version to 4.7.0 (no changes needed).
    - Reformat for readability.
  * debian/copyright:
    - Add myself to debian/*.
    - Add separate stanza with dual license for files in debian/ where I am the
      sole author.
    - Update upstream homepage.
    - Update upstream copyright.
    - Reformat for readability.
  * debian/watch:  Bump version from 3 to 4.

 -- Soren Stoutner <soren@debian.org>  Fri, 27 Sep 2024 11:56:44 -0700

python-mnemonic (0.19-3) unstable; urgency=medium

  * Team upload.
  * Remove retired uploader.
  * Update standards version to 4.6.1, no changes needed.

 -- Bastian Germann <bage@debian.org>  Thu, 13 Jul 2023 15:59:31 +0200

python-mnemonic (0.19-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 15:24:47 -0400

python-mnemonic (0.19-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Tristan Seligmann ]
  * New upstream release.
  * Declare rootless build.
  * Bump debhelper-compat to 13.
  * Bump Standards-Version to 4.5.0 (no changes).
  * Switch to dh-sequence-*.
  * Expand upstream metadata.
  * Add autodep8 tests.

 -- Tristan Seligmann <mithrandi@debian.org>  Tue, 21 Jul 2020 10:14:20 +0200

python-mnemonic (0.18-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #937925
  * Set DPMT as Maintainer, Richard and Tristan as Uploaders

 -- Sandro Tosi <morph@debian.org>  Sun, 22 Dec 2019 15:10:15 -0500

python-mnemonic (0.18-1) unstable; urgency=medium

  * New upstream release.
  * Build Python package.
  * Switch to pybuild.
  * Raise debhelper compat level to 9.
  * Bump Standards-Version to 4.1.2 (no changes).

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 11 Dec 2017 11:05:07 +0200

python-mnemonic (0.15-1) unstable; urgency=medium

  * new upstream version without changelog

 -- Richard Ulrich <richi@paraeasy.ch>  Mon, 05 Sep 2016 22:16:16 +0200

python-mnemonic (0.12-1) unstable; urgency=low

  * source package automatically created by stdeb 0.6.0+git
  * Initial release. (Closes: #762985)

 -- Richard Ulrich <richi@paraeasy.ch>  Wed, 24 Sep 2014 23:27:39 +0200
